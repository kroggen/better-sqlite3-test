'use strict';
const fs = require('fs');

if (fs.existsSync('binn/makefile')) {
  console.log('the binn folder already exists')
} else {
  console.log('cloning binn...')
  const { execSync } = require('child_process');
  // stderr is sent to stderr of parent process
  execSync('git clone --depth=1 https://github.com/liteserver/binn', { stdio: 'inherit' });
}
